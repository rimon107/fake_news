from .textpreprocessing import textpreprocessing
from keras.preprocessing.sequence import pad_sequences
from keras_self_attention import SeqSelfAttention
from keras.models import load_model
from sklearn.externals import joblib
import numpy as np
import pickle
import os
import time

dict_metric_accuracy = {
'combined_CNN': 65.60,
'combined_LSTM': 68.48,
'combined_MLP':68.87,
'combined_NB': 67.17,
'combined_OM': 67.43,
'liarliar_CNN': 56.94,
'liarliar_LSTM': 58.19,
'liarliar_MLP': 61.11,
'liarliar_NB': 59.63,
'liarliar_OM': 59.52,
'liarliar_SVM': 57.87,
'mcintire_CNN': 93.37,
'mcintire_LSTM': 89.03,
'mcintire_MLP': 94.16,
'mcintire_NB': 89.58,
'mcintire_OM': 94.63,
'mcintire_SVM': 89.58
}
dict_metric_confidence = {
'combined_CNN': '64.93-66.28',
'combined_LSTM': '67.82-69.14',
'combined_MLP': '68.21-69.53',
'combined_NB': '66.51-67.84',
'combined_OM': '66.77-68.10',
'liarliar_CNN': '56.08-57.79',
'liarliar_LSTM': '57.33-59.04',
'liarliar_MLP': '60.27-61.96',
'liarliar_NB': '52.88-60.48',
'liarliar_OM': '58.66-60.37',
'liarliar_SVM': '57.02-58.73',
'mcintire_CNN': '92.76-93.98',
'mcintire_LSTM': '88.26-89.80',
'mcintire_MLP': '93.58-94.74',
'mcintire_NB': '88.83-90.33',
'mcintire_OM': '94.08-95.19',
'mcintire_SVM': '88.83-90.33'
}


def classify_data(dataset,news):
    input_sentence = textpreprocessing(news)
    print(input_sentence)

    print("input end")



    modelSet = ["NB", "SVM", "MLP", "CNN", "LSTM", "OM"]

    cur_path = os.path.dirname(__file__)
    savedModel = 0

    Result = []


    for model in modelSet:
        modelFilename = "models/" + dataset + "_" + model
        modelPath = cur_path + "/" + modelFilename

        if (model == "NB" or model == "SVM"):
            savedModel = joblib.load(modelPath)
        else:
            savedModel = load_model(modelPath, custom_objects={'SeqSelfAttention': SeqSelfAttention})

        if (model == "NB" or model == "SVM" or model == "MLP"):
            tokenizerFileName = "vectorizer/" + dataset + "Weak"
            tokenizerPath = cur_path + "/" + tokenizerFileName + ".pkl"
            file = open(tokenizerPath, "rb")
            tokenizer = pickle.load(file)
            input = tokenizer.transform([input_sentence]).toarray()
        else:
            tokenizerFileName = "vectorizer/" + dataset + "Strong"
            tokenizerPath = cur_path + "/" + tokenizerFileName + ".pkl"
            file = open(tokenizerPath, "rb")
            tokenizer = pickle.load(file)
            input = tokenizer.texts_to_sequences([input_sentence])

            MAX_SEQUENCE_LENGTH = 500
            MAX_NUM_WORDS = 25000

            word_index = tokenizer.word_index
            num_words = min(MAX_NUM_WORDS, len(word_index)) + 1
            input = pad_sequences(input,
                                 maxlen=MAX_SEQUENCE_LENGTH,
                                 padding='pre',
                                 truncating='pre')

            #print(input)

        startTime = time.time()
        score = savedModel.predict(input)[0]
        endTime = time.time()
        #print(score)

        classification = None
        if(score>=0.5):
            classification = "FAKE"
        else:
            classification = "REAL"

        # Result[model]=[classification,endTime-startTime]
        print(model,"done!!")
        res = {
            'model': model,
            'result': classification,
            'time': endTime-startTime,
            'accuracy': dict_metric_accuracy[dataset+'_'+model],
            'confidence': dict_metric_confidence[dataset+'_'+model]
        }
        Result.append(res)

    return Result



# r = classify_data("mcintire", "Trump is a stupid piece of shit. Not good president!!")
# print(r)